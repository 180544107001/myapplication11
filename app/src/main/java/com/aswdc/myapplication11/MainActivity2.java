package com.aswdc.myapplication11;


import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity2 extends AppCompatActivity {

    TextView tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);
        initViewReference();
        bindViewValues();
    }

    void initViewReference() {
        tvDisplay = findViewById(R.id.tvactDisplay);
    }

    void bindViewValues() {
        String value = getIntent().getStringExtra("answer");
        tvDisplay.setText(value);
    }

}

