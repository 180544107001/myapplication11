package com.aswdc.myapplication11;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private long backPressedTime;

    EditText etFirstname, etLastname, PhoneNumber, Email;
    Button btnClcik;
    ImageView ivClose;
    CheckBox chbCricket, chbPubg, chbValorant;
    RadioGroup rbmale, rbfemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        intViewEvent();
        setTypefaceOnView();
    }

    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(getBaseContext(), "Press Back Again To Exit", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }


    void initViewReference() {
        etFirstname = findViewById(R.id.etactFirstname);
        etLastname = findViewById(R.id.etactLastname);
        btnClcik = findViewById(R.id.btnactClick);
        ivClose = findViewById(R.id.ivactClose);
        PhoneNumber = findViewById(R.id.etactPhoneNumber);
        Email = findViewById(R.id.etactEmail);
        chbCricket = findViewById(R.id.chbactcricket);
        chbPubg = findViewById(R.id.chbactpubg);
        chbValorant = findViewById(R.id.chbactvalorant);


    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "Fonts/Alata-Regular.ttf");
        etFirstname.setTypeface(typeface);
        etLastname.setTypeface(typeface);
        PhoneNumber.setTypeface(typeface);
        Email.setTypeface(typeface);
        btnClcik.setTypeface(typeface);
        chbValorant.setTypeface(typeface);
        chbCricket.setTypeface(typeface);
        chbPubg.setTypeface(typeface);
    }

    void intViewEvent() {

        btnClcik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    etFirstname.requestFocus();
                    etLastname.requestFocus();
                    PhoneNumber.requestFocus();
                    Email.requestFocus();
                    Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                    String contactTempString = etFirstname.getText().toString() + " " + etLastname.getText().toString();
                    intent.putExtra("answer", getEmailPatten(contactTempString));
                    startActivity(intent);
                }
            }

        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    boolean isValid() {
        boolean flag = true;

        //frirstname validation

        if (TextUtils.isEmpty(etFirstname.getText())) {
            etFirstname.setError(getString(R.string.error_Enter_Value));
            etFirstname.requestFocus();
            flag = false;
            return false;
        } else {
            String name = etFirstname.getText().toString().trim();
            String namepatten = "[A-Z][a-zA-Z]*";
            if (!name.matches(namepatten)) {
                etFirstname.setError("Enter Valid First Name(FistLetter Must Be Capital)");
                etFirstname.requestFocus();
                flag = false;
                return false;
            }
        }
        //lastname validation
        if (TextUtils.isEmpty(etLastname.getText())) {
            etLastname.setError(getString(R.string.error_Enter_Value));
            etLastname.requestFocus();
            flag = false;
            return false;
        } else {
            String name = etLastname.getText().toString().trim();
            String namepatten = "[A-Z][a-zA-Z]*";
            if (!name.matches(namepatten)) {
                etLastname.setError("Enter Valid Last Name(FistLetter Must Be Capital)");
                etLastname.requestFocus();
                flag = false;
                return false;
            }
        }
        

        //Phone Number Validation
        if (TextUtils.isEmpty(PhoneNumber.getText())) {
            PhoneNumber.setError(getString(R.string.error_Enter_Value));
            PhoneNumber.requestFocus();
            flag = false;
            return false;
        } else {
            String Number = PhoneNumber.getText().toString();
            if (PhoneNumber.length() < 10) {
                PhoneNumber.setError("Enter Valid Number");
                PhoneNumber.requestFocus();
                flag = false;
                return false;
            }
        }
        //Email Adress Validation

        if (TextUtils.isEmpty(Email.getText())) {
            Email.setError(getString(R.string.error_Enter_Value));
            Email.requestFocus();
            flag = false;
            return false;
        } else {
            String email = Email.getText().toString();
            String EmailPatten = "^[a-zA-Z0-9_+&-]+(?:\\.[a-zA-Z0-9_+&-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
            if (!getEmailPatten(email).matches(getEmailPatten(EmailPatten))) {
                Email.setError("Enter Valid Email Address");
                Email.requestFocus();
                flag = false;
                return false;
            }
        }

        //CheckBOX Validation
        if (!(chbPubg.isChecked() || chbCricket.isChecked() || chbValorant.isChecked())) {
            Toast.makeText(this, "Please Check Any One Hobbies", Toast.LENGTH_LONG).show();
            flag = false;
        }
        return flag;
    }

    private String getEmailPatten(String emailPatten) {
        return emailPatten;
    }
}




